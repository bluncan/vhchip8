# Generate a rom for testing purposes
REG_REG_INSTRUCTIONS = [
    '8120', #ld v1, v2
    '8241', #or v2, v4
    '82e2', #and v2, v14
    '8153', #xor v1, v5
    '8364', #add v3, v6
    '8365', #sub v3, v6
    '8786', #shr v7, v8?
    '8237', #subn v2, v3
    '8e1E' #shl v14, v1
    ]

REG_IMM_INSTRUCTIONS = [
    "7280", #Add V2, 0x80
    "C3FF" # RND V3, 0xFF
    ]

LOAD_STORE_INSTR = [
    "63FF", #Load V3, 0xFF
    "A123", #LD I, 0x123
    "F407", #LD V4, DT
    "F715", #LD DT, V7
    "F818", #LD ST, V8
    "F61E", #ADD I, V6,
    "F733", # LD B, V7
    "F355", # LD [I], V3
    "A100", # LD I, 0x100
    "F365", # LD V3, [I]
    "F00A", # LD V0, Key
    "F029" # LD F, V0
]

BRANCH_INSTR = [
    "1002", #JP 0x002
    "2004", #Call 0x004
    "5230", #SE V2, V3
    "9230", #SNE V2, V3
    "B006", #JP V0 + 005
    "00EE"  #RET
]

instr = []
for i in LOAD_STORE_INSTR:
    instr.append(i[0:2])
    instr.append(i[-2:])

print('(')
for i in instr:
    print(f'    X"{i}",')

print('    others => (others => \'0\')\n);')