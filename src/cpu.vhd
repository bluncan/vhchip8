library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use STD.TEXTIO.ALL;

library WORK;
use WORk.common.ALL;

entity cpu is
  Port (
      clk: in std_logic;
      reset: in std_logic;
      tick_cpu: in std_logic;
      tick_timer: in std_logic;
      gpu_ready: in std_logic;
      keys: in std_logic_vector(15 downto 0);
      -- memory
      mem_read: out std_logic;
      mem_read_addr: out unsigned(11 downto 0);
      mem_read_data: in unsigned(7 downto 0);
      mem_read_ack: in std_logic;
      mem_write: out std_logic;
      mem_write_addr: out unsigned(11 downto 0);
      mem_write_data: out unsigned(7 downto 0);
      -- gpu
      gpu_cmd: out GpuCommand;
      gpu_draw_offset: out unsigned(11 downto 0);
      gpu_draw_x: out unsigned(5 downto 0);
      gpu_draw_y: out unsigned(4 downto 0);
      gpu_draw_length: out unsigned(3 downto 0);
      gpu_cmd_submitted: out std_logic;
      gpu_collision: in std_logic
   );
end cpu;

architecture Behavioral of cpu is
    signal pc_i: unsigned(15 downto 0):= (others => '0');
    signal reg_i: unsigned(15 downto 0):= (others => '0');

    signal dt_i: unsigned(7 downto 0):= (others => '0');
    signal st_i: unsigned(7 downto 0):= (others => '0');

    signal instruction_i: unsigned(15 downto 0):= (others => '0');
    signal x_i: unsigned(3 downto 0):= (others => '0');
    signal y_i: unsigned(3 downto 0):= (others => '0');
    signal n_i: unsigned(3 downto 0):= (others => '0');
    signal kk_i: unsigned(7 downto 0):= (others => '0');
    signal nnn_i: unsigned(11 downto 0):= (others => '0');

    -- I register
    signal enable_i_reg: std_logic := '0';
    signal next_i_reg_i: unsigned(15 downto 0) := (others => '0');
    -- bcd
    signal bcd_input_i: unsigned(7 downto 0):= (others => '0');
    signal bcd_hundreds_i: unsigned(3 downto 0) := (others => '0');
    signal bcd_tens_i: unsigned(3 downto 0) := (others => '0');
    signal bcd_ones_i: unsigned(3 downto 0) := (others => '0');
    -- rng
    signal rng_value_i: unsigned(7 downto 0) := (others => '0');

    type cpu_states is (
        FETCH_INSTRUCTION_HIGH,
        FETCH_INSTRUCTION_LOW,
        DECODE_INSTRUCTION,
        WAIT_CLK,
        CHECK_ZERO,
        STORE_CARRY_REG,
        STORE_VX_REG,
        STORE_I_REG,
        INCREMENT_PC,
        JUMP_ADDRESS,
        JUMP_RELATIVE,
        SKIP_INSTRUCTION,
        WRITE_STACK,
        READ_STACK,
        STORE_BCD1,
        STORE_BCD2,
        STORE_BCD3,
        STORE_MEMORY_LOOP,
        STORE_MEMORY,
        LOAD_MEMORY,
        LOAD_MEMORY_STORE,
        HALT_UNTILL_PRESS,
        SUBMIT_GPU_CMD,
        WAIT_FOR_GPU,
        STORE_COLLISION,
        WAIT_CYCLE
    );
    
    signal state_i: cpu_states := WAIT_CLK;
    signal wait_state_i: cpu_states := WAIT_CLK;
    signal opcode_i: Opcode;
    signal memory_counter_i: unsigned(7 downto 0);

    -- register file
    signal register_file_write_enable: std_logic;
    signal register_file_read_addr1: unsigned(3 downto 0):= (others => '0');
    signal register_file_read_addr2: unsigned(3 downto 0):= (others => '0');
    signal register_file_write_data: unsigned(7 downto 0):= (others => '0');
    signal register_file_read_data1: unsigned(7 downto 0):= (others => '0');
    signal register_file_read_data2: unsigned(7 downto 0):= (others => '0');
    signal register_file_write_addr: unsigned(3 downto 0):= (others => '0');
    -- reg file register
    signal register_file_read_data1_sync: unsigned(7 downto 0):= (others => '0');
    signal register_file_read_data2_sync: unsigned(7 downto 0):= (others => '0');
    signal reg_file_read_registers: std_logic:= '0';

    -- alu 
    signal alu_data1: unsigned(7 downto 0):= (others => '0');
    signal alu_data2: unsigned(7 downto 0):= (others => '0');
    signal alu_op: ALUOp;
    signal alu_out: unsigned(7 downto 0):= (others => '0');
    signal alu_carry: std_logic:= '0';
    signal alu_zero: std_logic:= '0';

    -- stack
    signal stack_enable: std_logic:= '0';
    signal stack_in: unsigned(15 downto 0):= (others => '0');
    signal stack_out: unsigned(15 downto 0):= (others => '0');
    signal stack_write_enable: std_logic:= '0';
    signal stack_full: std_logic:= '0';
    signal stack_empty: std_logic:= '0';

    --dt / st
    signal dt_in: unsigned(7 downto 0) := (others => '0');
    signal st_in: unsigned(7 downto 0) := (others => '0');
    signal dt_load: std_logic := '0';
    signal st_load: std_logic := '0';
    signal dt_zero: std_logic;
    signal st_zero: std_logic;

    --instruction
    signal read_instruction_low: std_logic:= '0';
    signal read_instruction_high: std_logic:= '0';

    -- pc register
    signal pc_in: unsigned(15 downto 0);
    signal pc_enable: std_logic := '0';
    -- key encoder
    signal encoded_key: unsigned(7 downto 0) := (others => '0');
    signal encoded_key_valid: std_logic:= '0';


begin

    instruction_decoder_c: entity WORK.instruction_decoder port map (
        data_in => instruction_i,
        instruction => opcode_i,
        nnn => nnn_i,
        n => n_i,
        x => x_i,
        y => y_i,
        kk => kk_i
    );

    alu_control_c: entity WORK.alu_control port map (
        opcode_in => opcode_i,
        alu_op => alu_op
    );

    stack_c: entity WORK.stack port map (
        clk => clk,
        reset => reset,
        enable => stack_enable,
        data_in => stack_in,
        data_out => stack_out,
        write_enable => stack_write_enable,
        full => stack_full,
        empty => stack_empty
    );

    stack_in <= pc_i;
    stack_write_enable <= '1' when (state_i = WRITE_STACK) else '0';
    stack_enable <= '1' when (state_i = WRITE_STACK or state_i = READ_STACK) else '0';

    register_file_c: entity WORK.register_file port map (
        clk => clk,
        read_addr1 => register_file_read_addr1,
        read_addr2 => register_file_read_addr2,
        write_addr => register_file_write_addr,
        write_data => register_file_write_data,
        write_enable => register_file_write_enable,
        read_data1 => register_file_read_data1,
        read_data2 => register_file_read_data2
    );

    -- Registers for memoring the output of the register file
    rf_read_data1_sync_c: entity WORK.generic_register port map (
        clk => clk,
        reset => reset,
        data_in => register_file_read_data1,
        data_out => register_file_read_data1_sync,
        enable => reg_file_read_registers
    );

    rf_read_data2_sync_c: entity WORK.generic_register port map (
        clk => clk,
        reset => reset,
        data_in => register_file_read_data2,
        data_out => register_file_read_data2_sync,
        enable => reg_file_read_registers
    );
    reg_file_read_registers <= '1' when (state_i = DECODE_INSTRUCTION or state_i = STORE_MEMORY) else '0';
    
    
    register_file_write_data <= "0000000" & alu_carry when (state_i = STORE_CARRY_REG) else
                                "0000000" & gpu_collision when (state_i = STORE_COLLISION) else 
                                mem_read_data when (state_i = LOAD_MEMORY_STORE) else
                                encoded_key when (state_i = STORE_VX_REG and opcode_i = LD_KEY) else
                                alu_out;

    register_file_write_enable <= '1' when ((state_i = STORE_VX_REG) or (state_i = STORE_CARRY_REG) or (state_i = LOAD_MEMORY_STORE and mem_read_ack = '1') or (state_i = STORE_COLLISION)) else '0';
    register_file_write_addr <= X"F" when (state_i = STORE_CARRY_REG) else
                                X"F" when (state_i = STORE_COLLISION) else
                                memory_counter_i(3 downto 0) when (state_i = LOAD_MEMORY_STORE) else
                                x_i;

    with opcode_i select register_file_read_addr1 <=
        y_i when SUBN_REG,
        X"0" when JP_REG,
        y_i when LD_REG,
        memory_counter_i(3 downto 0) when LD_I_REGS,
        x_i when others;

    with opcode_i select register_file_read_addr2 <=
        x_i when SUBN_REG,
        y_i when others;

    -- ALU stuff
    alu_c: entity WORK.alu port map (
        data_a => alu_data1,
        data_b => alu_data2,
        alu_op => alu_op,
        data_out => alu_out,
        carry => alu_carry,
        zero => alu_zero
    );

    with opcode_i select alu_data1 <=
        kk_i when LD_BYTE,
        rng_value_i when RND,
        dt_i when LD_REG_DT,
        register_file_read_data1_sync when others;

    with opcode_i select alu_data2 <=
        kk_i when ADD_BYTE,
        kk_i when SE_BYTE,
        kk_i when SNE_BYTE,
        kk_i when RND,
        register_file_read_data2_sync when others;

    -- DT / ST stuff
    dt_counter_c: entity WORK.generic_counter port map (
        clk => clk,
        reset => reset,
        data_in => dt_in,
        enable => tick_timer,
        data_out => dt_i,
        zero => dt_zero,
        load => dt_load
    );

    st_counter_c: entity WORK.generic_counter port map (
        clk => clk,
        reset => reset,
        data_in => st_in,
        enable => tick_timer,
        data_out => st_i,
        zero => dt_zero,
        load => st_load
    );
    
    dt_load <= '1' when (opcode_i = LD_DT_REG) else '0';
    st_load <= '1' when (opcode_i = LD_ST_REG) else '0';
    dt_in <= register_file_read_data1_sync; --when (opcode_i = LD_DT_REG) else (others => '0');
    st_in <= register_file_read_data1_sync; --when (opcode_i = LD_ST_REG) else (others => '0');

    -- I register
    i_reg_c: entity WORK.generic_register
        generic map (WIDTH => 16)
        port map (
            clk => clk,
            enable => enable_i_reg,
            reset => reset,
            data_in => next_i_reg_i,
            data_out => reg_i
        );
        
    enable_i_reg <= '1' when (state_i = STORE_I_REG) else '0';

    with opcode_i select next_i_reg_i <=
        X"0" & nnn_i when LD_ADDR,
        reg_i + register_file_read_data1_sync when ADD_I_REG,
        register_file_read_data1_sync * 5 when LD_I_REG,
        (others => '0') when others; 

    -- Instruction register
    instruction_reg_low_c: entity WORK.generic_register port map (
        clk => clk,
        reset => reset,
        enable => read_instruction_low,
        data_in => mem_read_data,
        data_out => instruction_i(15 downto 8)
    );

    instruction_reg_high_c: entity WORK.generic_register port map (
        clk => clk,
        reset => reset,
        enable => read_instruction_high,
        data_in => mem_read_data,
        data_out => instruction_i(7 downto 0)
    );

    read_instruction_low <= '1' when (state_i = FETCH_INSTRUCTION_LOW and mem_read_ack = '1') else '0';
    read_instruction_high <= '1' when (state_i = FETCH_INSTRUCTION_HIGH and mem_read_ack = '1') else '0';
    
    -- pc register
    pc_register_c: entity WORK.generic_register
        generic map (WIDTH => 16, INIT_VALUE => 512)
        port map (
            clk => clk,
            reset => reset,
            enable => pc_enable,
            data_in => pc_in,
            data_out => pc_i
        );

        pc_in <= X"0" & nnn_i when (state_i = JUMP_ADDRESS and (opcode_i = JP_ADDR or opcode_i = CALL_ADDR)) else
                (X"00" & register_file_read_data1_sync) + (X"0" & nnn_i) when (state_i = JUMP_RELATIVE) else
                pc_i + 2 when (state_i = SKIP_INSTRUCTION) else
                stack_out when (state_i = JUMP_ADDRESS and opcode_i = RET) else
                pc_i + 2;

        pc_enable <= '1' when (state_i = JUMP_ADDRESS) else
                     '1' when (state_i = JUMP_RELATIVE) else
                     '1' when (state_i = SKIP_INSTRUCTION) else
                     '1' when (state_i = INCREMENT_PC) else
                     '0';

    process(clk)
    begin
        if (RISING_EDGE(clk)) then
            if (reset = '1') then
                state_i <= WAIT_CLK;
            else
                case (state_i) is
                    when FETCH_INSTRUCTION_LOW =>
                        if (mem_read_ack = '1') then
                            wait_state_i <= FETCH_INSTRUCTION_HIGH;
                            state_i <= WAIT_CYCLE;
                        end if;

                    when FETCH_INSTRUCTION_HIGH =>
                        if (mem_read_ack = '1') then
                            wait_state_i <= INCREMENT_PC;
                            state_i <= WAIT_CYCLE;
                        end if;

                    when DECODE_INSTRUCTION =>
                        state_i <= WAIT_CLK;
                        wait_state_i <= WAIT_CLK;

                        case (opcode_i) is
                            when CLS =>
                                gpu_cmd <= GPU_CLEAR;
                                state_i <= SUBMIT_GPU_CMD;

                            when RET =>
                                state_i <= READ_STACK;

                            when JP_ADDR =>
                                wait_state_i <= JUMP_ADDRESS;
                                state_i <= WAIT_CYCLE;

                            when CALL_ADDR =>
                                state_i <= WRITE_STACK;

                            when SE_BYTE =>
                                state_i <= CHECK_ZERO;

                            when SNE_BYTE =>
                                state_i <= CHECK_ZERO;

                            when SE_REG =>
                                state_i <= CHECK_ZERO;

                            when LD_BYTE =>
                                state_i <= STORE_VX_REG;

                            when ADD_BYTE =>
                                state_i <= STORE_VX_REG;

                            when LD_REG =>
                                state_i <= STORE_VX_REG;

                            when OR_REG =>
                                state_i <= STORE_VX_REG;

                            when AND_REG =>
                                state_i <= STORE_VX_REG;

                            when XOR_REG =>
                                state_i <= STORE_VX_REG;

                            when ADD_REG =>
                                state_i <= STORE_CARRY_REG;

                            when SUB_REG =>
                                state_i <= STORE_CARRY_REG;

                            when SHR =>
                                state_i <= STORE_CARRY_REG;

                            when SUBN_REG =>
                                state_i <= STORE_CARRY_REG;

                            when SHL =>
                                state_i <= STORE_CARRY_REG;

                            when SNE_REG =>
                                state_i <= CHECK_ZERO;

                            when LD_ADDR =>
                                state_i <= STORE_I_REG;

                            when JP_REG =>
                                wait_state_i <= JUMP_RELATIVE;
                                state_i <= WAIT_CYCLE;
                            
                            when RND =>
                                state_i <= STORE_VX_REG;
                                
                            when DRW =>
                                gpu_cmd <= GPU_DRAW;
                                state_i <= SUBMIT_GPU_CMD;

                            when SKP_KEY =>
                                if (keys(to_integer(register_file_read_data1_sync)) = '1') then
                                    state_i <= SKIP_INSTRUCTION;
                                end if;

                            when SKNP_KEY =>
                                if (keys(to_integer(register_file_read_data1_sync)) = '0') then
                                    state_i <= SKIP_INSTRUCTION;
                                end if;

                            when LD_REG_DT =>
                                state_i <= STORE_VX_REG;

                            when LD_KEY =>
                                state_i <= HALT_UNTILL_PRESS;

                            when LD_DT_REG => null;

                            when LD_ST_REG => null;

                            when ADD_I_REG =>
                                state_i <= STORE_I_REG;

                            when LD_I_REG =>
                                state_i <= STORE_I_REG;

                            when LD_BCD_REG =>
                                state_i <= STORE_BCD1;

                            when LD_I_REGS =>
                                memory_counter_i <= X"0" & x_i + 1;
                                state_i <= STORE_MEMORY_LOOP;

                            when LD_REGS_I =>
                                memory_counter_i <= X"0" & x_i;
                                state_i <= LOAD_MEMORY;
                                
                            when others => null;
                        end case;
                    
                    when WAIT_CLK =>
                        wait_state_i <= WAIT_CLK;
                        if (tick_cpu = '1') then
                            memory_counter_i <= (others => '0');
                            state_i <= FETCH_INSTRUCTION_LOW;
                        end if;

                    when INCREMENT_PC =>
                        wait_state_i <= DECODE_INSTRUCTION;
                        state_i <= WAIT_CYCLE;

                    when CHECK_ZERO =>
                        wait_state_i <= WAIT_CLK;
                        if (((opcode_i = SE_BYTE) or (opcode_i = SE_REG)) and alu_zero = '1') then
                            state_i <= SKIP_INSTRUCTION;
                        elsif (((opcode_i = SNE_BYTE) or (opcode_i = SNE_REG)) and alu_zero = '0') then
                            state_i <= SKIP_INSTRUCTION;
                        else
                            state_i <= WAIT_CLK;
                        end if;
                        
                    when STORE_VX_REG =>
                        state_i <= WAIT_CLK;
                        wait_state_i <= WAIT_CLK;

                    when STORE_CARRY_REG =>
                        state_i <= STORE_VX_REG;
                        wait_state_i <= WAIT_CLK;

                    when STORE_I_REG =>
                        state_i <= WAIT_CLK;
                        wait_state_i <= WAIT_CLK;

                    when READ_STACK =>
                        state_i <= JUMP_ADDRESS;
                        wait_state_i <= WAIT_CLK;

                    when WRITE_STACK =>
                        state_i <= JUMP_ADDRESS;
                        wait_state_i <= WAIT_CLK;

                    when JUMP_ADDRESS =>
                        state_i <= WAIT_CLK;
                        wait_state_i <= WAIT_CLK;
                    
                    when JUMP_RELATIVE =>
                        state_i <= WAIT_CLK;
                        wait_state_i <= WAIT_CLK;

                    when SKIP_INSTRUCTION =>
                        state_i <= WAIT_CLK;
                        wait_state_i <= WAIT_CLK;
                    
                    when STORE_BCD1 =>
                        state_i <= STORE_BCD2;
                        wait_state_i <= WAIT_CLK;

                    when STORE_BCD2 =>
                        state_i <= STORE_BCD3;
                        wait_state_i <= WAIT_CLK;

                    when STORE_BCD3 =>
                        state_i <= WAIT_CLK;
                        wait_state_i <= WAIT_CLK;

                    when STORE_MEMORY_LOOP =>
                        memory_counter_i <= memory_counter_i - 1;
                        state_i <= STORE_MEMORY;
                        wait_state_i <= WAIT_CLK;

                    when STORE_MEMORY =>
                        wait_state_i <= WAIT_CLK;
                        if (memory_counter_i = 0) then
                            state_i <= WAIT_CLK;
                        else
                            state_i <= STORE_MEMORY_LOOP;
                        end if;

                    when LOAD_MEMORY =>
                        state_i <= LOAD_MEMORY_STORE;
                        wait_state_i <= WAIT_CLK;

                    when LOAD_MEMORY_STORE =>
                        wait_state_i <= WAIT_CLK;
                        if (mem_read_ack = '1') then
                            if (memory_counter_i = 0) then
                                state_i <= WAIT_CLK;
                            else
                                memory_counter_i <= memory_counter_i - 1;
                                state_i <= LOAD_MEMORY;
                            end if;
                        end if;

                    when HALT_UNTILL_PRESS =>
                        if (encoded_key_valid = '1') then
                            state_i <= STORE_VX_REG;
                            wait_state_i <= WAIT_CLK;
                        else
                            wait_state_i <= HALT_UNTILL_PRESS;
                            state_i <= WAIT_CYCLE;
                        end if;

                    when SUBMIT_GPU_CMD =>
                        wait_state_i <= WAIT_CLK;
                        state_i <= WAIT_FOR_GPU;

                    when WAIT_FOR_GPU =>
                        wait_state_i <= WAIT_CLK;
                        if (gpu_ready = '1') then
                            state_i <= STORE_COLLISION;
                        end if;

                    when STORE_COLLISION =>
                        state_i <= WAIT_CLK;
                        wait_state_i <= WAIT_CLK;

                    when WAIT_CYCLE =>
                        state_i <= wait_state_i;
                        wait_state_i <= WAIT_CLK;
                end case;
            end if;
        end if;
    end process;

    with state_i select mem_write_data <=
        register_file_read_data1 when STORE_MEMORY,
        X"0" & bcd_hundreds_i when STORE_BCD1,
        X"0" & bcd_tens_i when STORE_BCD2,
        X"0" & bcd_ones_i when STORE_BCD3,
        (others => '0') when others;

    with state_i select mem_write_addr <=
        reg_i(11 downto 0) when STORE_BCD1,
        reg_i(11 downto 0) + 1 when STORE_BCD2,
        reg_i(11 downto 0) + 2 when STORE_BCD3,
        reg_i(11 downto 0) + (X"0" & memory_counter_i) when STORE_MEMORY,
        (others => '0') when others;

    with state_i select mem_read_addr <=
        pc_i(11 downto 0) when FETCH_INSTRUCTION_LOW,
        pc_i(11 downto 0) + 1 when FETCH_INSTRUCTION_HIGH,
        reg_i(11 downto 0) + (X"0" & memory_counter_i) when LOAD_MEMORY,
        reg_i(11 downto 0) + (X"0" & memory_counter_i) when LOAD_MEMORY_STORE,
        (others => '0') when others;

    mem_write <= '1' when (state_i = STORE_MEMORY or state_i = STORE_BCD1 or state_i = STORE_BCD2 or state_i = STORE_BCD3) else '0';
    mem_read <= '1' when (state_i = FETCH_INSTRUCTION_LOW or state_i = FETCH_INSTRUCTION_HIGH or state_i = LOAD_MEMORY) else '0';
    
    gpu_draw_x <= register_file_read_data1_sync(5 downto 0);
    gpu_draw_y <= register_file_read_data2_sync(4 downto 0);
    gpu_draw_offset <= reg_i(11 downto 0);
    gpu_draw_length <= n_i;
    gpu_cmd_submitted <= '1' when (state_i = SUBMIT_GPU_CMD) else '0';
    

    bcd_input_i <= register_file_read_data1_sync;
    bcd_c: entity WORK.bcd port map (
        bcd_input => bcd_input_i,
        digit2 => bcd_hundreds_i,
        digit1 => bcd_tens_i,
        digit0 => bcd_ones_i
    );
    
    -- RNG
    rng_c: entity WORK.rng port map (
        clk => clk,
        reset => '0',
        enable => tick_cpu,
        data_out => rng_value_i
    );

    -- key encoder
    key_encoder_c: entity WORK.key_encoder port map (
        clk => clk,
        keys => keys,
        value => encoded_key,
        valid => encoded_key_valid
    );

end Behavioral;
