library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

library WORK;
use WORK.common.ALL;

entity instruction_decoder is
  Port (
      data_in: in unsigned(15 downto 0);
      instruction: out Opcode;
      nnn: out unsigned(11 downto 0);
      n: out unsigned(3 downto 0);
      x: out unsigned(3 downto 0);
      y: out unsigned(3 downto 0);
      kk: out unsigned(7 downto 0)
   );
end instruction_decoder;

architecture Behavioral of instruction_decoder is
    signal op_i: unsigned(3 downto 0);
    signal nnn_i: unsigned(11 downto 0);
    signal n_i: unsigned(3 downto 0);
    signal x_i: unsigned(3 downto 0);
    signal y_i: unsigned(3 downto 0);
    signal kk_i: unsigned(7 downto 0);
    signal instruction_i: Opcode;
begin
    op_i <= data_in(15 downto 12);
    nnn_i <= data_in(11 downto 0);
    n_i <= data_in(3 downto 0);
    x_i <= data_in(11 downto 8);
    y_i <= data_in(7 downto 4);
    kk_i <= data_in(7 downto 0);

    decode: process(data_in, op_i, n_i, kk_i)
    begin
        case op_i is
            when X"0" =>
                if (data_in = X"00E0") then
                    instruction_i <= CLS;
                elsif (data_in = X"00EE") then
                    instruction_i <= RET;
                else
                    instruction_i <= SYS;
                end if;
            when X"1" =>
                instruction_i <= JP_ADDR;
            when X"2" =>
                instruction_i <= CALL_ADDR;
            when X"3" =>
                instruction_i <= SE_BYTE;
            when X"4" =>
                instruction_i <= SNE_BYTE;
            when X"5" =>
                instruction_i <= SE_REG;
            when X"6" =>
                instruction_i <= LD_BYTE;
            when X"7" =>
                instruction_i <= ADD_BYTE;
            when X"8" =>
                case n_i is
                    when X"0" =>
                        instruction_i <= LD_REG;
                    when X"1" => 
                        instruction_i <= OR_REG;
                    when X"2" =>
                        instruction_i <= AND_REG;
                    when X"3" =>
                        instruction_i <= XOR_REG;
                    when X"4" =>
                        instruction_i <= ADD_REG;
                    when X"5" =>
                        instruction_i <= SUB_REG;
                    when X"6" =>
                        instruction_i <= SHR;
                    when X"7" =>
                        instruction_i <= SUBN_REG;
                    when X"E" =>
                        instruction_i <= SHL;
                    when others =>
                        instruction_i <= UNDEFINED;
                end case;
            when X"9" =>
                instruction_i <= SNE_REG;
            when X"A" =>
                instruction_i <= LD_ADDR;
            when X"B" =>
                instruction_i <= JP_REG;
            when X"C" =>
                instruction_i <= RND;
            when X"D" =>
                instruction_i <= DRW;
            when X"E" =>
                if (kk_i = X"9E") then
                    instruction_i <= SKP_KEY;
                elsif (kk_i = X"A1") then
                    instruction_i <= SKNP_KEY;
                else
                    instruction_i <= UNDEFINED;
                end if;
            when X"F" =>
                case kk_i is
                    when X"07" =>
                        instruction_i <= LD_REG_DT;
                    when X"0A" =>
                        instruction_i <= LD_KEY;
                    when X"15" =>
                        instruction_i <= LD_DT_REG;
                    when X"18" =>
                        instruction_i <= LD_ST_REG;
                    when X"1E" =>
                        instruction_i <= ADD_I_REG;
                    when X"29" =>
                        instruction_i <= LD_I_REG;
                    when X"33" =>
                        instruction_i <= LD_BCD_REG;
                    when X"55" =>
                        instruction_i <= LD_I_REGS;
                    when X"65" =>
                        instruction_i <= LD_REGS_I;
                    when others =>
                        instruction_i <= UNDEFINED;
                end case;
                
                when others =>
                    instruction_i <= UNDEFINED;
        end case;
    end process; -- decode

    instruction <= instruction_i;
    nnn <= nnn_i;
    x <= x_i;
    y <= y_i;
    n <= n_i;
    kk <= kk_i;
end Behavioral;
