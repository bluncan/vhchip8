library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

package common is
    constant RAM_LENGTH: integer := 4096;
    constant GPU_FRAMEBUFFER_LENGTH: integer:= 256;
    constant GPU_FRAMEBUFFER_OFFSET: integer:= 256;
    constant GPU_FRAMEBUFFER_OFFSET_CONCAT: unsigned(3 downto 0):= "0001";
    constant GPU_FRAMEBUFFER_END: integer:= GPU_FRAMEBUFFER_OFFSET + GPU_FRAMEBUFFER_LENGTH;

    constant GPU_FRAMEBUFFER_WIDTH: integer := 64;
    constant GPU_FRAMEBUFFER_HEIGHT: integer := 32;

    constant GAME_ROM_LENGTH: integer := 3584;
    
    type GpuCommand is (
        GPU_CLEAR,
        GPU_DRAW
    );

    type Opcode is (
        UNDEFINED,
        NOP,
        SYS, -- 0nnn
        CLS, -- 00E0
        RET, -- 00EE
        JP_ADDR, -- 1nnn
        CALL_ADDR, -- 2nnn
        SE_BYTE, -- 3xkk
        SNE_BYTE, -- 4xkk
        SE_REG, -- 5xy0
        LD_BYTE, -- 6xkk
        ADD_BYTE, -- 7xkk
        LD_REG, -- 8xy0
        OR_REG, -- 8xy1
        AND_REG, -- 8xy2
        XOR_REG, -- 8xy3
        ADD_REG, -- 8xy4
        SUB_REG, -- 8xy5
        SHR,     -- 8xy6
        SUBN_REG,-- 8xy7
        SHL,     -- 8xyE
        SNE_REG, -- 9xy0
        LD_ADDR, -- Annn
        JP_REG, -- Bnnn
        RND,    -- Cxkk
        DRW,    -- Dxyn
        SKP_KEY, -- Ex9E
        SKNP_KEY, -- ExA1
        LD_REG_DT,-- Fx07
        LD_KEY,   -- Fx0A
        LD_DT_REG,-- Fx15
        LD_ST_REG,-- Fx18
        ADD_I_REG,-- Fx1E
        LD_I_REG, -- Fx29
        LD_BCD_REG, -- Fx33
        LD_I_REGS, -- Fx55
        LD_REGS_I -- Fx65
    );

    type ALUOp is (
        PASS_A,
        PASS_B,
        ADD,
        SUB,
        LEFT_SHIFT_ONE,
        RIGHT_SHIFT_ONE,
        BITWISE_AND,
        BITWISE_OR,
        BITWISE_XOR
    );

    --type CounterMode is (
        --ASCENDING,
        --DESCENDING
    --);


    function get_vram_offset_for_fb(x: integer; y: integer)
            return integer;

    function get_offset_for_fb(x: integer; y: integer)
            return integer;
    
end package common;

package body common is
    function get_vram_offset_for_fb(x: integer; y: integer)
            return integer is
    begin
        return ((y rem GPU_FRAMEBUFFER_HEIGHT) * 8) + ((x rem GPU_FRAMEBUFFER_WIDTH) / 8);
    end get_vram_offset_for_fb;

    function get_offset_for_fb(x: integer; y: integer)
            return integer is
    begin
        return (GPU_FRAMEBUFFER_OFFSET + get_vram_offset_for_fb(x, y));
    end get_offset_for_fb;
 
end package body common;
