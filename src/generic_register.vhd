library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity generic_register is
    generic (
        WIDTH : integer := 8;
        INIT_VALUE: integer := 0
    );
    
    Port (
        clk: in std_logic;
        enable: in std_logic;
        reset: in std_logic;
        data_in: in unsigned(WIDTH - 1 downto 0);
        data_out: out unsigned(WIDTH - 1 downto 0)  
     );
end generic_register;

architecture Behavioral of generic_register is
    signal data_i: unsigned(WIDTH - 1 downto 0) := to_unsigned(INIT_VALUE, WIDTH);
begin 
    process(clk)
    begin
        if (RISING_EDGE(clk)) then
            if (reset = '1') then
                data_i <= (others => '0');
            elsif (enable = '1') then
                data_i <= data_in;
            end if;
        end if;
    end process ;
    
    data_out <= data_i;
end Behavioral;