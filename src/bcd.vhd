library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity bcd is
  port (
    bcd_input: in unsigned(7 downto 0);
    digit2: out unsigned(3 downto 0);
    digit1: out unsigned(3 downto 0);
    digit0: out unsigned(3 downto 0)
  );
end bcd;
    
architecture Behavioral of bcd is
	signal p_i : unsigned (9 downto 0) := (others => '0');
begin
	process( bcd_input )
    	variable z_i: unsigned(17 downto 0) := (others => '0');
    begin
        z_i := (others => '0');
		z_i(10 downto 3) := bcd_input;
		for i in 0 to 4 loop
			if (z_i(11 downto 8) > 4)	then
				z_i(11 downto 8) := z_i(11 downto 8) + 3;
			end if;
			if (z_i(15 downto 12) > 4) then
				z_i(15 downto 12) := z_i(15 downto 12) + 3;
			end if;
			z_i(17 downto 1) := z_i(16 downto 0);
		end loop;
		p_i <= z_i(17 downto 8);
	end process ;
	
	digit2 <= "00" & p_i(9 downto 8);
	digit1 <= p_i(7 downto 4);
	digit0 <= p_i(3 downto 0);
end Behavioral;
