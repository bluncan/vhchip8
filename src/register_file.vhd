library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity register_file is
  Port (
      clk: in std_logic;
      read_addr1: in unsigned(3 downto 0);
      read_addr2: in unsigned(3 downto 0);
      write_addr: in unsigned(3 downto 0);
      write_data: in unsigned(7 downto 0);
      write_enable: in std_logic;
      read_data1: out unsigned(7 downto 0);
      read_data2: out unsigned(7 downto 0)
   );
end register_file;

architecture Behavioral of register_file is
    type reg_array_i is array(0 to 15) of unsigned(7 downto 0);
    signal reg_file_i: reg_array_i := (
        X"00",
        X"00",
        x"00",
        x"00",
        x"00",
        x"00",
        x"00",
        x"00",
        x"00",
        x"00",
        x"00",
        x"00",
        x"00",
        x"00",
        X"00",
        X"00"
    );
begin
    process( clk )
    begin
        if (RISING_EDGE(clk)) then
            if (write_enable = '1') then
                reg_file_i(to_integer(write_addr)) <= write_data;
            end if;
        end if;
    end process ;

    read_data1 <= reg_file_i(to_integer(read_addr1));
    read_data2 <= reg_file_i(to_integer(read_addr2));

end Behavioral;
