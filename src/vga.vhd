library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

library WORK;
use WORK.common.ALL;

entity vga is
    Port ( clk: in std_logic;
           enable: in std_logic;
           sw: in std_logic_vector(15 downto 0);
           h_sync: out std_logic;
           v_sync: out std_logic;
           red: out std_logic_vector(3 downto 0);
           green: out std_logic_vector(3 downto 0);
           blue: out std_logic_vector(3 downto 0);

           memory_addr: out unsigned(7 downto 0);
           memory_data: in unsigned(7 downto 0)
    );
end vga;

architecture Behavioral of vga is
    -- vga signals
    signal h_counter_i: unsigned(15 downto 0) := (others => '0');
    signal v_counter_i: unsigned(15 downto 0) := (others => '0');
    signal enable_v_counter_i: std_logic := '0';
    signal active_area_i: std_logic := '0';
    signal color_i: std_logic_vector(3 downto 0):= (others => '0');
    signal pixel_i: unsigned(0 downto 0) := (others => '0');
    signal pixel_x_i: unsigned(15 downto 0):= (others => '0');
    signal pixel_y_i: unsigned(15 downto 0) := (others => '0');

    --signal pixel_scaler_x: unsigned(3 downto 0) := (others => '0');
    --signal pixel_scaler_y: unsigned(3 downto 0) := (others => '0');

-- https://electronics.stackexchange.com/questions/228825/programming-pattern-to-generate-vga-signal-with-micro-controller
-- http://tinyvga.com/vga-timing/640x480@60Hz

    -- Horizontal Pixel Timings
    constant H_FRONT_PORCH: integer := 16;
    constant H_SYNC_PULSE: integer := 96;
    constant H_BACK_PORCH: integer := 48;
    constant H_ACTIVE_VIDEO: integer := 640;
    constant H_TOTAL_PIXELS: integer := 800;

    -- Vertical Pixel Timings
    constant V_FRONT_PORCH: integer := 10;
    constant V_SYNC_PULSE: integer := 2;
    constant V_BACK_PORCH: integer := 33;
    constant V_ACTIVE_VIDEO: integer := 480;
    constant V_TOTAL_LINES : integer := 525;

    constant X_PADDING: integer := 64;
    constant Y_PADDING: integer := 112;

    signal x_i: unsigned(15 downto 0);
    signal y_i: unsigned(15 downto 0);

begin

    x_i <= (h_counter_i - (H_BACK_PORCH + H_SYNC_PULSE));
    y_i <= (v_counter_i - (V_BACK_PORCH + V_SYNC_PULSE));

    pixel_x_i <= x_i / 10;
    pixel_y_i <= y_i / 15;

    horizontal_counter : process( clk )
    begin
    if (RISING_EDGE(clk)) then
        if (enable = '1') then
                if (h_counter_i < H_TOTAL_PIXELS - 1) then
                    h_counter_i <= h_counter_i + 1;
                    enable_v_counter_i <= '0';
                else
                    h_counter_i <= (others => '0');
                    enable_v_counter_i <= '1';
                end if;
            end if;
        end if;
    end process; -- horizontal_counter
    
    vertical_counter : process( clk )
    begin
        if (RISING_EDGE(clk)) then
            if (enable = '1') then
                if (enable_v_counter_i = '1') then
                    if (v_counter_i < V_TOTAL_LINES - 1) then
                        v_counter_i <= v_counter_i + 1;
                    else
                        v_counter_i <= (others => '0');
                    end if;
                end if;
            end if;
        end if;
    end process; -- vertical_counter

    is_in_active_area: process( clk )
    begin
        if (RISING_EDGE(clk)) then
            if (enable = '1') then
                if (h_counter_i < H_TOTAL_PIXELS - H_FRONT_PORCH and h_counter_i > H_BACK_PORCH + H_SYNC_PULSE and 
                    v_counter_i < V_TOTAL_LINES - V_FRONT_PORCH and v_counter_i > V_BACK_PORCH + V_SYNC_PULSE) then
                    active_area_i <= '1';
                else
                    active_area_i <= '0';
                end if;
            end if;
        end if;
    end process; -- is_in_active_area

    process(clk)
    begin
        if (RISING_EDGE(clk)) then
            memory_addr <= to_unsigned(get_vram_offset_for_fb(to_integer(pixel_x_i), to_integer(pixel_y_i)), memory_addr'length);
            if (enable = '1') then
                if (active_area_i = '1') then
                        pixel_i(0) <= memory_data(7 - (to_integer(pixel_x_i) mod 8));
                    else
                        pixel_i(0) <= '0';
                end if;
            end if;
        end if;
    end process;

    process(clk)
    begin
        if (RISING_EDGE(clk)) then
            if (enable = '1') then
                if (pixel_i(0) = '1' and active_area_i = '1') then
                    red <= (not sw(15)) & (not sw(14)) & (not sw(13)) & (not sw(12));
                    green <= (not sw(11)) & (not sw(10)) & (not sw(9)) & (not sw(8));
                    blue <= (not sw(7)) & (not sw(6)) & (not sw(5)) & (not sw(4));
                else
                    red <= "0000";
                    green <= "0000";
                    blue <= "0000";
                end if;
            end if;
        end if;
    end process;

    h_sync <= '1' when (h_counter_i < H_SYNC_PULSE) else '0';
    v_sync <= '1' when (v_counter_i < V_SYNC_PULSE) else '0';

end Behavioral;
