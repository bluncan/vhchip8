library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

library WORK;
use WORK.common.ALL;

entity chip8 is
  Port (
      reset: in std_logic;
      clk: in std_logic;
      col: out std_logic_vector(3 downto 0);
      row: in std_logic_vector(3 downto 0);
      red: out std_logic_vector(3 downto 0);
      blue: out std_logic_vector(3 downto 0);
      green: out std_logic_vector(3 downto 0);
      v_sync: out std_logic;
      h_sync: out std_logic;
      sw: in std_logic_vector(15 downto 0)
   );
end chip8;

architecture Behavioral of chip8 is
    -- gpu
    signal gpu_cmd: GpuCommand;
    signal gpu_draw_offset: unsigned(11 downto 0) := (others => '0');
    signal gpu_draw_x: unsigned(5 downto 0);
    signal gpu_draw_y: unsigned(4 downto 0);
    signal gpu_draw_length: unsigned(3 downto 0);
    signal gpu_cmd_submitted, gpu_collision, gpu_ready: std_logic;

    -- memory
    signal mem_read: std_logic;
    signal mem_read_addr: unsigned(11 downto 0) := (others => '0');
    signal mem_read_data: unsigned(7 downto 0) := (others => '0');
    signal mem_read_ack, mem_write: std_logic;
    signal mem_write_addr: unsigned(11 downto 0);
    signal mem_write_data: unsigned(7 downto 0);
    
    -- gpu
    signal gpu_mem_read: std_logic;
    signal gpu_mem_read_addr: unsigned(11 downto 0);
    signal gpu_mem_read_data: unsigned(7 downto 0) := (others => '0');
    signal gpu_mem_read_ack, gpu_mem_write: std_logic;
    signal gpu_mem_write_addr: unsigned(11 downto 0);
    signal gpu_mem_write_data: unsigned(7 downto 0);

    --timer
    signal tick_cpu: std_logic;
    signal tick_25: std_logic;
    signal tick_timer: std_logic;

    --vga
    signal vga_mem_addr: unsigned(7 downto 0);
    signal vga_mem_data: unsigned(7 downto 0) := (others => '0');

    -- keyboard
    signal keys: std_logic_vector(15 downto 0);

    -- bcd to 7 segment signals
    signal hex_i: std_logic_vector(15 downto 0) := (others => '0');
begin
    cpu_c: entity WORK.cpu port map (
        reset => reset,
        clk => clk,
        tick_cpu => tick_cpu,
        tick_timer => tick_timer,
        gpu_ready => gpu_ready,
        gpu_cmd => gpu_cmd,
        gpu_draw_length => gpu_draw_length,
        gpu_draw_offset => gpu_draw_offset,
        gpu_draw_x => gpu_draw_x,
        gpu_draw_y => gpu_draw_y,
        gpu_cmd_submitted => gpu_cmd_submitted,
        gpu_collision => gpu_collision,
        mem_read => mem_read,
        mem_read_addr => mem_read_addr,
        mem_read_data => mem_read_data,
        mem_read_ack => mem_read_ack,
        mem_write => mem_write,
        mem_write_addr => mem_write_addr,
        mem_write_data => mem_write_data,
        keys => keys
    );

    gpu_c: entity WORK.gpu port map (
        clk => clk,
        reset => reset,
        gpu_ready => gpu_ready,
        gpu_cmd => gpu_cmd,
        gpu_draw_length => gpu_draw_length,
        gpu_draw_offset => gpu_draw_offset,
        gpu_draw_x => gpu_draw_x,
        gpu_draw_y => gpu_draw_y,
        gpu_cmd_submitted => gpu_cmd_submitted,
        gpu_collision => gpu_collision,
        gpu_mem_read => gpu_mem_read,
        gpu_mem_read_addr => gpu_mem_read_addr,
        gpu_mem_read_ack => gpu_mem_read_ack,
        gpu_mem_read_data => gpu_mem_read_data,
        gpu_mem_write => gpu_mem_write,
        gpu_mem_write_addr => gpu_mem_write_addr,
        gpu_mem_write_data => gpu_mem_write_data
    );

    memory_c: entity WORK.memory port map (
        clk => clk,
        read => mem_read,
        read_addr => mem_read_addr,
        read_data => mem_read_data,
        read_ack => mem_read_ack,
        write => mem_write,
        write_addr => mem_write_addr,
        write_data => mem_write_data,
        gpu_read => gpu_mem_read,
        gpu_read_addr => gpu_mem_read_addr,
        gpu_read_data => gpu_mem_read_data,
        gpu_read_ack => gpu_mem_read_ack,
        gpu_write => gpu_mem_write,
        gpu_write_addr => gpu_mem_write_addr,
        gpu_write_data => gpu_mem_write_data,
        vga_addr => vga_mem_addr,
        vga_data => vga_mem_data
    );

    timer_c: entity WORK.timer
    port map (
        clk => clk,
        tick_vga => tick_25,
        tick_cpu => tick_cpu,
        tick_timer => tick_timer
    );

    keyboard_c: entity WORK.keyboard 
        port map (
            clk => clk,
            reset => '0',
            col => col,
            row => row,
            keys_pressed => keys
        );

    vga_c: entity WORK.vga port map (
        clk => clk,
        sw => sw,
        enable => tick_25,
        h_sync => h_sync,
        v_sync => v_sync,
        red => red,
        green => green,
        blue => blue,
        memory_addr => vga_mem_addr,
        memory_data => vga_mem_data
    );
end Behavioral;
