library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

library WORK;
use WORK.common.ALL;

entity alu is
    Port (
        data_a: in unsigned(7 downto 0);
        data_b: in unsigned(7 downto 0);
        alu_op: in ALUOp;
        data_out: out unsigned(7 downto 0);
        zero: out std_logic;
        carry: out std_logic
    );
    
end alu;

architecture Behavioral of alu is
    signal result_i: unsigned(8 downto 0);
begin
    process(data_a, data_b, alu_op)
    begin
        case alu_op is
            when PASS_A =>
                result_i <= '0' & data_a;
                carry <= '0';
            when PASS_B =>
                result_i <= '0' & data_b;
                carry <= '0';
            when ADD =>
                result_i <= ('0' & data_a) + ('0' & data_b);
                if (to_integer(data_a) + to_integer(data_b) >= 256) then
                    carry <= '1';
                else
                    carry <= '0';
                end if;
            when SUB =>
                result_i <= ('0' & data_a) - ('0' & data_b);
                if (to_integer(data_a) > to_integer(data_b)) then
                    carry <= '1';
                else
                    carry <= '0';
                end if;
            when LEFT_SHIFT_ONE =>
                result_i <= '0' & shift_left(data_a, 1);
                carry <= data_a(7);
            when RIGHT_SHIFT_ONE =>
                result_i <= '0' & shift_right(data_a, 1);
                carry <= data_a(0);
            when BITWISE_AND =>
                result_i <= '0' & (data_a and data_b);
                carry <= '0';
            when BITWISE_OR =>
                result_i <= '0' & (data_a or data_b);
                carry <= '0';
            when BITWISE_XOR =>
                result_i <= '0' & (data_a xor data_b);
                carry <= '0';
        end case;
    end process;
        
    data_out <= result_i(7 downto 0);
    zero <= '1' when (result_i(7 downto 0) = 0) else '0';
        
end Behavioral ; -- Behavioral


