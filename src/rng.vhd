library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity rng is
  port (
    clk: in std_logic;
    reset: in std_logic;
    enable: in std_logic;
    data_out: out unsigned(7 downto 0)
  ) ;
end rng;

architecture Behavioral of rng is
    constant SEED: unsigned(7 downto 0) := x"AC";

    signal data_i: unsigned(7 downto 0) := SEED;
    signal d_i: unsigned(0 downto 0) := "0";
begin
    lfsr : process( clk, reset )
    begin
        if (RISING_EDGE(clk)) then
            if (reset = '1') then
                data_i <= SEED;
                d_i <= "0";
            elsif (enable = '1') then
                d_i(0) <= (data_i(7) xnor data_i(5) xnor data_i(4) xnor data_i(3));
                data_i <= d_i & data_i(7 downto 1);
            end if;
        end if;
    end process ; -- lfsr

    data_out <= data_i;
end Behavioral ;