LIBRARY ieee;
USE ieee.std_logic_1164.all;

entity keyboard IS
	generic(
		clk_freq: integer := 100_000_000;
		stable_time: integer := 10); 
	port(
		clk: in std_logic;
		reset: in std_logic;
		row: in std_logic_vector(3 downto 0); 
		col: out std_logic_vector(3 downto 0); 
		keys_pressed: out std_logic_vector(15 downto 0));
end keyboard;

architecture Behavioral of keyboard is
    signal rows_int_i: std_logic_vector(3 downto 0);
    signal col_i: std_logic_vector(3 downto 0);
    signal keys_int_i: std_logic_vector(15 downto 0) := (others => '0');
	signal keys_stored_i: std_logic_vector(15 downto 0) := (others => '0');
begin
	process(clk)
    begin
		if (RISING_EDGE(clk)) then      
			rows_int_i <= row;
        end if;
	end process;

	process(clk, reset)
        variable count: integer range 0 to clk_freq / 3_300_000 := 0;
        variable presses: integer range 0 to 16 := 0; 
	begin
		 if (reset = '1') then
			col_i <= (others => '1');
			keys_int_i <= (others => '0');
        elsif (rising_edge(clk)) then
		    if (count < clk_freq / 3_300_000) then
				count := count + 1;
			else
				count := 0;
				case col_i is
					when "1111" =>
						presses := 0;
						press_count : for i in 0 to 15 loop
							if (keys_int_i(i) = '1') then
								presses := presses + 1;
							end if;
						end loop press_count;
						if (presses > 0) then 
							keys_stored_i <= keys_int_i;
						else
                            keys_stored_i <= (others => '0');
						end if;
                        keys_int_i <= (others => '0');
						col_i <= "0111";
                    
                    -- single keys
					when "0111" =>
                        keys_int_i(1) <= not rows_int_i(3);
                        keys_int_i(4) <= not rows_int_i(2);
                        keys_int_i(7) <= not rows_int_i(1);
                        keys_int_i(10) <= not rows_int_i(0);
						col_i <= "1011";

					when "1011" =>
                        keys_int_i(2) <= not rows_int_i(3);
                        keys_int_i(5) <= not rows_int_i(2);
                        keys_int_i(8) <= not rows_int_i(1);
                        keys_int_i(0) <= not rows_int_i(0);
						col_i <= "1101";

					when "1101" =>
                        keys_int_i(3) <= not rows_int_i(3);
                        keys_int_i(6) <= not rows_int_i(2);
                        keys_int_i(9) <= not rows_int_i(1);
                        keys_int_i(11) <= not rows_int_i(0);
						col_i <= "1110";

					when "1110" =>
                        keys_int_i(12) <= not rows_int_i(3);
                        keys_int_i(13) <= not rows_int_i(2);
                        keys_int_i(14) <= not rows_int_i(1);
                        keys_int_i(15) <= not rows_int_i(0);
						col_i <= "1111";
                
					when others =>
                        col_i <= "1111";   
				end case;
			end if;
		end if;
    end process;
    col <= col_i;

	row_debounce : for i in 15 downto 0 generate
		debounce_keys : entity WORK.debounce
			GENERIC MAP(clk_freq => clk_freq, stable_time => stable_time)
			PORT MAP(
                clk => clk, 
                reset => reset,
                button => keys_stored_i(i), 
                result => keys_pressed(i)
            );
	end generate;
end Behavioral;