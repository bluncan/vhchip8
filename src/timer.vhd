library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity timer is
    generic (CLK_FREQUENCY: integer := 100_000_000);
    Port(
        clk: in std_logic;
        tick_vga: out std_logic;
        tick_timer: out std_logic;
        tick_cpu: out std_logic
    );
end timer;

architecture Behavioral of timer is
    constant CPU_SPEED: integer:= 500;
    constant VGA_SPEED: integer:= 25_000_000;
    constant TIMER_SPEED: integer:= 60;

    constant CPU_TOP: integer:= (CLK_FREQUENCY / CPU_SPEED) - 1;
    constant VGA_TOP: integer:= (CLK_FREQUENCY / VGA_SPEED) - 1;
    constant TIMER_TOP: integer:= (CLK_FREQUENCY / TIMER_SPEED) - 1;

    signal counter_vga_i: unsigned(31 downto 0):= to_unsigned(VGA_TOP, 32);
    signal counter_cpu_i: unsigned(31 downto 0):= to_unsigned(CPU_TOP, 32);
    signal counter_timer_i: unsigned(31 downto 0):= to_unsigned(TIMER_TOP, 32);
begin

    process(clk)
    begin
        if (RISING_EDGE(clk)) then
            if (counter_cpu_i = 0) then
                counter_cpu_i <= to_unsigned(CPU_TOP, counter_cpu_i'length);
                tick_cpu <= '1';
            else
                counter_cpu_i <= counter_cpu_i - 1;
                tick_cpu <= '0';
            end if;

            if (counter_vga_i = 0) then
                counter_vga_i <= to_unsigned(VGA_TOP, counter_vga_i'length);
                tick_vga <= '1';
            else
                counter_vga_i <= counter_vga_i - 1;
                tick_vga <= '0';
            end if;

            if (counter_timer_i = 0) then
                counter_timer_i <= to_unsigned(TIMER_TOP, counter_timer_i'length);
                tick_timer <= '1';
            else
                counter_timer_i <= counter_timer_i - 1;
                tick_timer <= '0';
            end if;
        end if;
    end process;

    --tick_vga <= '1' when counter_vga_i = 0 else '0';
    --tick_cpu <= '1' when counter_cpu_i = 0 else '0';
    --tick_timer <= '1' when counter_timer_i = 0 else '0';

end Behavioral; 