library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library WORK;
use WORK.common.all;

entity stack is
    generic (CAPACITY: integer := 16);
    port(
        clk     : in  std_logic;
        reset     : in  std_logic;
        enable: in std_logic;
        data_in  : in  unsigned(15 downto 0); -- input Data 
        data_out  : out unsigned(15 downto 0); -- output Data 
        write_enable: in std_logic;
        full  : out std_logic; -- 1 when memory is full.
        empty :     out std_logic -- 1 when memory is empty.
    );
end stack;

architecture Behavioral of stack is
    type memory_type_i is array (0 to CAPACITY-1) of unsigned(15 downto 0);
    signal memory_i : memory_type_i := (others => (others => '0'));
begin
    process(clk) is
        variable stack_pointer_i: integer range 0 to CAPACITY := 0;
        variable is_empty_i, is_full_i: std_logic := '0';
    begin
        if (RISING_EDGE(clk)) then
            if (reset = '1') then
                for i in 0 to CAPACITY-1 loop
                    memory_i(i) <= (others => '0');
                end loop;
                is_empty_i := '1';
                is_full_i  := '0';
                stack_pointer_i := 0;
            elsif (enable = '1') then
                if (write_enable = '1') then
                    if stack_pointer_i < CAPACITY then
                        stack_pointer_i := stack_pointer_i + 1;
                        memory_i(stack_pointer_i - 1) <= data_in;
                    end if;
                else
                    if is_empty_i = '0' then
                        data_out <= memory_i(stack_pointer_i - 1);
                        stack_pointer_i := stack_pointer_i - 1;
                    end if;
                end if;

                -- Check for Empty
                if stack_pointer_i = 0 then
                    is_empty_i := '1';
                else
                    is_empty_i := '0';
                end if;

                -- Check for Full
                if stack_pointer_i = CAPACITY then
                    is_full_i := '1';
                else
                    is_full_i := '0';
                end if;
            end if;
        end if;

        full  <= is_full_i;
        empty <= is_empty_i;
    end process;
end Behavioral;

