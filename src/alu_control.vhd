library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

library WORK;
use WORK.common.ALL;

entity alu_control is
  Port (
      opcode_in: in Opcode;
      alu_op: out ALUOp
   );
end alu_control;

architecture Behavioral of alu_control is

begin
    process(opcode_in)
    begin
        case opcode_in is
            when LD_REG =>  -- 8xy0
                alu_op <= PASS_A; 
            when OR_REG => -- 8xy1
                alu_op <= BITWISE_OR;
            when AND_REG => -- 8xy2
                alu_op <= BITWISE_AND;
            when XOR_REG => -- 8xy3
                alu_op <= BITWISE_XOR;
            when ADD_REG => -- 8xy4
                alu_op <= ADD;
            when SUB_REG => -- 8xy5
                alu_op <= SUB;
            when SHR => -- 8xy6
                alu_op <= RIGHT_SHIFT_ONE;
            when SUBN_REG => -- 8xy7
                alu_op <= SUB;
            when SHL => -- 8xyE
                alu_op <= LEFT_SHIFT_ONE;

            -- Byte instructions
            when RND => -- Cxkk
                alu_op <= BITWISE_AND;  
            when ADD_BYTE => -- 7xkk 
                alu_op <= ADD;

            -- Branch instructions
            when SE_BYTE => -- 3xkk
                alu_op <= SUB;
            when SNE_BYTE => -- 4xkk
                alu_op <= SUB;
            when SNE_REG =>
                alu_op <= SUB;
            when SE_REG =>
                alu_op <= SUB;

            -- Load
            when LD_REG_DT =>
                alu_op <= PASS_A;
            
            when others =>
                alu_op <= PASS_A;
        end case;
    end process;

end Behavioral;
