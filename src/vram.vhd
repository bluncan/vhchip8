library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

library WORK;
use WORK.common.ALL;

entity vram is
  Port (
      clk: in std_logic;
      write_enable: in std_logic;
      write_addr: in unsigned(7 downto 0);
      read_addr: in unsigned(7 downto 0);
      data_out: out unsigned(7 downto 0);
      write_data: in unsigned(7 downto 0);
      vga_read_addr: in unsigned(7 downto 0);
      vga_read_data: out unsigned(7 downto 0)
   );
end vram;

architecture Behavioral of vram is
    type vram_memory is array(0 to GPU_FRAMEBUFFER_LENGTH-1) of unsigned(7 downto 0);
    signal vram_i: vram_memory := (others => (others => '0'));
begin

    process(clk)
    begin
        if (RISING_EDGE(clk)) then
            if (write_enable = '1') then
                    vram_i(to_integer(write_addr)) <= write_data;
            end if;
            data_out <= vram_i(to_integer(read_addr));
            vga_read_data <= vram_i(to_integer(vga_read_addr));
        end if;
    end process;

end Behavioral;
