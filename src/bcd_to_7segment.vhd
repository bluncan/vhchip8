library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity bcd_to_7segment is
      Port (
            clk: in std_logic;
            hex: in std_logic_vector(15 downto 0);
            led: out std_logic_vector(7 downto 0);
            anode: out std_logic_vector(3 downto 0)
      );
end bcd_to_7segment;

architecture Behavioral of bcd_to_7segment is
      signal refresh_counter_i: unsigned(19 downto 0) := (others => '0');
      signal led_select_i: unsigned(1 downto 0) := "00";
      signal led_bcd_i: std_logic_vector(3 downto 0);
begin
      digit_select: process( led_bcd_i )
      begin
            case led_bcd_i is
                  when "0001" => led <= "11111001";   --1
                  when "0010" => led <= "10100100";   --2
                  when "0011" => led <= "10110000";   --3
                  when "0100" => led <= "10011001";   --4
                  when "0101" => led <= "10010010";   --5
                  when "0110" => led <= "10000010";   --6
                  when "0111" => led <= "11111000";   --7
                  when "1000" => led <= "10000000";   --8
                  when "1001" => led <= "10010000";   --9
                  when "1010" => led <= "10001000";   --A
                  when "1011" => led <= "10000011";   --b
                  when "1100" => led <= "11000110";   --C
                  when "1101" => led <= "10100001";   --d
                  when "1110" => led <= "10000110";   --E
                  when "1111" => led <= "10001110";   --F
                  when others => led <= "11000000";   --0
            end case;                  
      end process; -- digit_select

      refresh_process: process( clk )
      begin
            if (RISING_EDGE(clk)) then
                  refresh_counter_i <= refresh_counter_i + 1;
            end if;
      end process ; -- refresh_process
      led_select_i <= refresh_counter_i(19 downto 18);

      anode_select: process( led_select_i, hex )
      begin
            case led_select_i is
                  when "00" => 
                        anode <= "0111";
                        led_bcd_i <= hex(15 downto 12);
                  when "01" => 
                        anode <= "1011";
                        led_bcd_i <= hex(11 downto 8);
                  when "10" => 
                        anode <= "1101";
                        led_bcd_i <= hex(7 downto 4);
                  when "11" => 
                        anode <= "1110";
                        led_bcd_i <= hex(3 downto 0);
            end case;
      end process ; -- anode_select
end Behavioral;
