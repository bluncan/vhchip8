library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity key_encoder is
  Port (
      clk: in std_logic;
      keys: in std_logic_vector(15 downto 0);
      value: out unsigned(7 downto 0);
      valid: out std_logic
   );
end key_encoder;

architecture Behavioral of key_encoder is
    signal value_i: unsigned(3 downto 0);
begin
    process(clk)
    variable found: std_logic := '0';
    begin
        if (RISING_EDGE(clk)) then
            found := '0';
            for i in 15 downto 0 loop
                if keys(i) = '1' then
                    found := '1';
                    value_i <= to_unsigned(i, value_i'length);
                end if;
            end loop;
    
            if (found = '1') then
                valid <= '1';
            else
                valid <= '0';
                value_i <= X"0";
            end if;
        end if;
    end process;

    value <= X"0" & value_i;

end Behavioral;
