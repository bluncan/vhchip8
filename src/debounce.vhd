LIBRARY ieee;
USE ieee.std_logic_1164.all;

entity debounce is
	generic(
		clk_freq    : integer := 100_000_000;
		stable_time : integer := 10); 
	Port(
		clk: in std_logic;
		reset: in std_logic;
		button: in std_logic;
		result: out std_logic);
end debounce;

architecture Behavioral of debounce is
	signal flipflops_i: std_logic_vector(1 downto 0);
	signal counter_set_i: std_logic;
begin

	counter_set_i <= flipflops_i(0) xor flipflops_i(1);

	process(clk, reset)
		variable count : integer range 0 to clk_freq * stable_time / 1000;
	begin
		if (reset = '1') then
			flipflops_i(1 downto 0) <= "00";
			result <= '0';
		elsif (RISING_EDGE(clk)) then 
			flipflops_i(0) <= button;     
			flipflops_i(1) <= flipflops_i(0); 
			if (counter_set_i = '1') then 
				count := 0;
			elsif (count < clk_freq * stable_time / 1000) then
				count := count + 1;
            else                  
				result <= flipflops_i(1);
			end if;
		end if;
	end process;

end Behavioral;