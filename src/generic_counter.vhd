
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

library WORK;
use WORK.common.ALL;

entity generic_counter is
    generic (WIDTH: integer := 8);
    Port (
      clk: in std_logic;
      enable: in std_logic;
      reset: in std_logic;
      load: in std_logic;
      data_in: in unsigned(WIDTH - 1 downto 0);
      zero: out std_logic;
      data_out: out unsigned(WIDTH - 1 downto 0)
   );
end generic_counter;

architecture Behavioral of generic_counter is
    signal data_i: unsigned(WIDTH - 1 downto 0) := (others => '0');
begin
    process(clk)
    begin
        if (RISING_EDGE(clk)) then
            if (reset = '1') then
                data_i <= (others => '0');
            elsif (load = '1') then
                data_i <= data_in;
            elsif (enable = '1') then
                if (data_i /= 0) then
                    data_i <= data_i - 1;
                end if;
            end if;
        end if;
    end process ;
    
    data_out <= data_i;
    zero <= '1' when data_i = 0 else '0';
end Behavioral;
