library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use STD.TEXTIO.ALL;

library WORK;
use WORK.common.ALL;

entity gpu is
  Port ( 
    clk: in std_logic;
    reset: in std_logic;
    gpu_cmd: in GpuCommand;
    gpu_draw_offset: in unsigned(11 downto 0);
    gpu_draw_length: in unsigned(3 downto 0);
    gpu_draw_x: in unsigned(5 downto 0);
    gpu_draw_y: in unsigned(4 downto 0);
    gpu_cmd_submitted: std_logic;
    gpu_collision: out std_logic;
    gpu_ready: out std_logic;
    -- memory
    gpu_mem_read: out std_logic;
    gpu_mem_read_addr: out unsigned(11 downto 0);
    gpu_mem_read_data: in unsigned(7 downto 0);
    gpu_mem_read_ack: in std_logic;
    gpu_mem_write: out std_logic;
    gpu_mem_write_addr: out unsigned(11 downto 0);
    gpu_mem_write_data: out unsigned(7 downto 0)
  );
end gpu;

architecture Behavioral of gpu is
    type gpu_states is (
        WAIT_FOR_CMD,
        DECODE_CMD,
        CLEAR,
        LOAD_SPRITE,
        LOAD_FB_LEFT,
        LOAD_FB_RIGHT,
        STORE_FB_LEFT,
        STORE_FB_RIGHT,
        FINISH_LINE
    );
    signal state_i: gpu_states:= WAIT_FOR_CMD;    
    signal clear_left_i: unsigned(7 downto 0):= (others => '0');
    signal lines_left_i: unsigned(7 downto 0):= (others => '0');
    signal use_right_i: std_logic := '0';
    signal shift_i: integer range 0 to 7 := 0;
    signal sprite_word_i: unsigned(15 downto 0):= (others => '0');
    signal sprite_offset_i: unsigned(11 downto 0):= (others => '0');
    signal gpu_collision_i: unsigned(7 downto 0) := (others => '0');
    signal extended_mem_data: unsigned(15 downto 0);
    
    signal address_left_i: unsigned(11 downto 0);
    signal address_right_i: unsigned(11 downto 0);
    signal screen_addr_i: unsigned(7 downto 0);
    signal screen_byte_i: unsigned(7 downto 0);

begin
    gpu_ready <= '1' when (state_i = WAIT_FOR_CMD) else '0';

    extended_mem_data <=  gpu_mem_read_data & x"00";
    address_left_i <= GPU_FRAMEBUFFER_OFFSET_CONCAT & screen_addr_i;
    address_right_i <= GPU_FRAMEBUFFER_OFFSET_CONCAT & screen_addr_i(7 downto 3) & (screen_addr_i(2 downto 0) + 1);

    process(clk)
    begin
        if (RISING_EDGE(clk)) then
            if (reset = '1') then
                state_i <= CLEAR;
            else
                case state_i is
                    when WAIT_FOR_CMD =>
                        if (gpu_cmd_submitted = '1') then
                            state_i <= DECODE_CMD;
                        end if;

                    when DECODE_CMD =>
                        case (gpu_cmd) is
                            when GPU_CLEAR =>
                                clear_left_i <= (others => '1');
                                state_i <= CLEAR;

                            when GPU_DRAW =>
                                gpu_collision_i <= (others => '0');
                                sprite_offset_i <= gpu_draw_offset;
                                lines_left_i <= (X"0" & gpu_draw_length) - 1;
                                screen_addr_i <= to_unsigned(to_integer(gpu_draw_y) * 8 + to_integer(gpu_draw_x) / 8, screen_addr_i'length);
                                --use_right_i?
                                if ((gpu_draw_x mod 8) /= 0) then
                                    use_right_i <= '1';
                                else
                                    use_right_i <= '0';
                                end if;
                                shift_i <= to_integer(gpu_draw_x) rem 8;
                                state_i <= LOAD_SPRITE;
                        end case;

                    when CLEAR =>
                        clear_left_i <= clear_left_i - 1;
                        if (clear_left_i = 0) then
                            state_i <= WAIT_FOR_CMD;
                        end if;

                    when LOAD_SPRITE =>
                        if (gpu_mem_read_ack = '1') then
                            sprite_word_i <= shift_right(extended_mem_data, shift_i);
                            state_i <= LOAD_FB_LEFT;                       
                        end if;

                    when LOAD_FB_LEFT =>
                        if (gpu_mem_read_ack = '1') then
                            screen_byte_i <= gpu_mem_read_data xor sprite_word_i(15 downto 8);
                            gpu_collision_i <= gpu_collision_i or (gpu_mem_read_data and sprite_word_i(15 downto 8));
                            state_i <= STORE_FB_LEFT;
                        end if;

                    when STORE_FB_LEFT =>
                        if (use_right_i = '1') then
                            state_i <= LOAD_FB_RIGHT;
                        else
                            state_i <= FINISH_LINE;
                        end if;

                    when LOAD_FB_RIGHT =>
                        if (gpu_mem_read_ack = '1') then
                            screen_byte_i <= gpu_mem_read_data xor sprite_word_i(7 downto 0);
                            gpu_collision_i <= gpu_collision_i or (gpu_mem_read_data and sprite_word_i(7 downto 0));
                            state_i <= STORE_FB_RIGHT;
                        end if;

                    when STORE_FB_RIGHT =>
                        state_i <= FINISH_LINE;
                
                    when FINISH_LINE =>
                        if (lines_left_i = 0) then
                            state_i <= WAIT_FOR_CMD;
                        else
                            lines_left_i <= lines_left_i - 1;
                            sprite_offset_i <= sprite_offset_i + 1;
                            screen_addr_i <= screen_addr_i + 8;
                            state_i <= LOAD_SPRITE;
                        end if;
                end case;
            end if;
        end if;
    end process;

    
    with state_i select gpu_mem_write_data <=
        X"00" when CLEAR,
        screen_byte_i when STORE_FB_LEFT,
        screen_byte_i when STORE_FB_RIGHT,
        (others => '0') when others;

    with state_i select gpu_mem_write_addr <=
        GPU_FRAMEBUFFER_OFFSET_CONCAT & clear_left_i when CLEAR,
        address_left_i when STORE_FB_LEFT,
        address_right_i when STORE_FB_RIGHT,
        (others => '0') when others;

    with state_i select gpu_mem_read_addr <=
        sprite_offset_i when LOAD_SPRITE,
        address_left_i when LOAD_FB_LEFT,
        address_right_i when LOAD_FB_RIGHT,
        (others => '0') when others;

    gpu_mem_write <= '1' when (state_i = CLEAR or state_i = STORE_FB_LEFT or state_i = STORE_FB_RIGHT) else '0';
    gpu_mem_read <= '1' when ((state_i = LOAD_SPRITE or state_i = LOAD_FB_LEFT or state_i = LOAD_FB_RIGHT) and gpu_mem_read_ack = '0') else '0';
    gpu_collision <= '1' when (gpu_collision_i /= 0) else '0';

end Behavioral;
