# VHChip8

A Chip-8 emulator implemented in VHDL.

Tested on a Digilent Basys3 board.

Tetris demo: https://drive.google.com/file/d/16KLtNobwbVUzJBRpx0BSF9ogolD3As-Y/view?usp=sharing

Pong demo: https://drive.google.com/file/d/16O9nJn_VBxOGEewQrQxmzuHsfQAn-XYT/view?usp=sharing

Keyboard demo: https://drive.google.com/file/d/16CZGHEMlIwtTzCRuEy4sZ7zAgoZOxGIm/view?usp=sharing

Sierpinski triangle demo: https://drive.google.com/file/d/16S9azlnKYSWifXMQGFoyCuXZTIMuKRI9/view?usp=sharing
