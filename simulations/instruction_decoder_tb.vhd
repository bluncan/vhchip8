library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

library WORK;
use WORK.common.ALL;

entity instruction_decoder_tb is

end entity instruction_decoder_tb;

architecture rtl of instruction_decoder_tb is
    type rom_array is array(0 to 34) of unsigned(15 downto 0);
    constant rom: rom_array := (
        X"0123",
        X"00e0",
        X"00ee",
        X"1123",
        X"2123",
        X"33ff",
        X"43ff",
        X"5360",
        X"63ff",
        X"73ff",
        X"8360",
        X"8361",
        X"8362",
        X"8363",
        X"8364",
        X"8365",
        X"8366",
        X"8367",
        X"836e",
        X"9360",
        X"a123",
        X"b123",
        X"c3ff",
        X"d368",
        X"e39e",
        X"e3a1",
        X"f307",
        X"f30a",
        X"f315",
        X"f318",
        X"f31e",
        X"f329",
        X"f333",
        X"f355",
        X"f365"
    );
    
    type opcode_array is array(0 to 34) of Opcode;
    constant expected_opcodes: opcode_array := (
        SYS, -- 0nnn
        CLS, -- 00E0
        RET, -- 00EE
        JP_ADDR, -- 1nnn
        CALL_ADDR, -- 2nnn
        SE_BYTE, -- 3xkk
        SNE_BYTE, -- 4xkk
        SE_REG, -- 5xy0
        LD_BYTE, -- 6xkk
        ADD_BYTE, -- 7xkk
        LD_REG, -- 8xy0
        OR_REG, -- 8xy1
        AND_REG, -- 8xy2
        XOR_REG, -- 8xy3
        ADD_REG, -- 8xy4
        SUB_REG, -- 8xy5
        SHR,     -- 8xy6
        SUBN_REG,-- 8xy7
        SHL,     -- 8xyE
        SNE_REG, -- 9xy0
        LD_ADDR, -- Annn
        JP_REG, -- Bnnn
        RND,    -- Cxkk
        DRW,    -- Dxyn
        SKP_KEY, -- Ex9E
        SKNP_KEY, -- ExA1
        LD_REG_DT,-- Fx07
        LD_KEY,   -- Fx0A
        LD_DT_REG,-- Fx15
        LD_ST_REG,-- Fx18
        ADD_I_REG,-- Fx1E
        LD_I_REG, -- Fx29
        LD_BCD_REG, -- Fx33
        LD_I_REGS, -- Fx55
        LD_REGS_I -- Fx65
    );

    signal instr: unsigned(15 downto 0);
    -- instruction decode
    signal instruction: Opcode;
    signal nnn: unsigned(11 downto 0);
    signal n: unsigned(3 downto 0);
    signal x: unsigned(3 downto 0);
    signal y: unsigned(3 downto 0);
    signal kk: unsigned(7 downto 0);

    constant WAIT_PERIOD : time := 10 ns;
begin

    instruction_decoder_c: entity WORK.instruction_decoder port map (
            data_in => instr,
            instruction => instruction,
            nnn => nnn,
            n => n,
            x => x,
            y => y,
            kk => kk
        );

    simulate: process
    variable errors: integer := 0;
    begin
        instr <= rom(0);
        wait for WAIT_PERIOD;
        for i in 0 to 34 loop
            instr <= rom(i);
            wait for WAIT_PERIOD;
            if (instruction /= expected_opcodes(i)) then
                errors := errors + 1;
                report "Wrong opcode for instruction: " & integer'image(i)
                   severity ERROR;
            end if;
        end loop;

        if (errors = 0) then
            report "Simulation succeded"
                severity NOTE;
        else
            report "Simulation failed with " & integer'image(errors) & " errors"
                severity ERROR;
        end if;
        
        wait;
    end process simulate;
end architecture;