library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;


entity register_tb is
end register_tb;

architecture Behavioral of register_tb is
    signal clk: std_logic;
    signal enable: std_logic;
    signal reset: std_logic;
    signal data_in: unsigned(7 downto 0);
    signal data_out: unsigned(7 downto 0);

    constant CLK_PERIOD: time := 10 ns;
begin
    comp: entity WORk.generic_register
            generic map (WIDTH => 8)
            port map (
                clk => clk,
                reset => reset,
                enable => enable,
                data_in => data_in,
                data_out => data_out
            );

    process
    begin
        clk <= '0';
        wait for CLK_PERIOD;
        clk <= '1';
        wait for CLK_PERIOD;
    end process;

    process
    begin
        enable <= '1';
        reset <= '1';
        wait for CLK_PERIOD;
        reset <= '0';
        data_in <= x"FF";
        wait for CLK_PERIOD;
        data_in <= x"0A";
        wait for CLK_PERIOD;

        wait;
    end process;


end Behavioral;
