library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity hex_to_bcd_tb is
end hex_to_bcd_tb;

architecture Behavioral of hex_to_bcd_tb is
    signal digit2, digit1, digit0: unsigned(3 downto 0);
    signal bcd_input: unsigned(7 downto 0) := X"80"; -- 128 
begin
    bcd_converter: entity WORK.bcd port map (
                   bcd_input => bcd_input,
                   digit2 => digit2,
                   digit1 => digit1,
                   digit0 => digit0
                   );
                   
     process
     begin
        bcd_input <= X"80";
        wait for 20ns;
        bcd_input <= X"ff";
        wait for 20ns;
        bcd_input <= X"10";
        wait for 20ns;
        bcd_input <= X"89";
        wait;
     end process;
end Behavioral;
