library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library WORK;
use WORK.common.all;

entity stack_tb is
end entity stack_tb;

architecture rtl of stack_tb is
    signal clk: std_logic;
    signal reset: std_logic;
    signal enable: std_logic := '0';
    signal data_in: unsigned(15 downto 0);
    signal data_out: unsigned(15 downto 0);
    signal write_enable: std_logic;
    signal full: std_logic;
    signal empty: std_logic;

    constant CLK_PERIOD: time := 6 ns;
begin
    stack_16: entity WORK.stack
        generic map (CAPACITY => 16)
        port map (
            clk => clk,
            reset => reset,
            enable => enable,
            write_enable => write_enable,
            data_in => data_in,
            data_out => data_out,
            full => full,
            empty => empty
        );

    gen_clk: process
    begin
        clk <= '0';
        wait for CLK_PERIOD / 2;
        clk <= '1';
        wait for CLK_PERIOD / 2;
    end process gen_clk;

    simulate: process
    variable errors: integer := 0;
    begin
        reset <= '1';
        wait for CLK_PERIOD;
        reset <= '0';

        if (empty /= '1') then
            errors := errors + 1;
            report "Stack should be empty"
                severity ERROR;
        end if;
        
        -- test writing
        enable <= '1';
        for i in 0 to 15 loop
            data_in <= to_unsigned(i, data_in'length);
            write_enable <= '1';
            wait for CLK_PERIOD;
        end loop;
        
        -- stack should be full
        if (full /= '1') then
            errors := errors + 1;
            report "Stack should be full"
                severity ERROR;
        end if;

        -- test reading
        enable <= '1';
        for i in 15 downto 0 loop
            write_enable <= '0';
            wait for CLK_PERIOD;
            if (to_integer(data_out) /= i) then
                errors := errors + 1;
                report "Read from stack: " & integer'image(to_integer(data_out)) & " expected: " & integer'image(i)
                    severity ERROR;
            end if;
        end loop;

        if (errors = 0) then
            report "Simulation succeded"
                severity NOTE;
        else
            report "Simulation failed with " & integer'image(errors) & " errors"
                severity ERROR;
        end if;
        
        wait;
    end process simulate;
end architecture;