library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;


entity tb_priority_encoder is
end tb_priority_encoder;

architecture Behavioral of tb_priority_encoder is
    signal keys_i: std_logic_vector(15 downto 0);
    signal valid_i: std_logic;
    signal value_i: unsigned(7 downto 0);
    signal clk_i: std_logic;
    
begin
    priority_encoder_c: entity WORK.key_encoder port map (
            clk => clk_i,
            keys => keys_i,
            valid => valid_i,
            value => value_i
        );
    
    process
    begin
        clk_i <= '0';
        wait for 10ns;
        clk_i <= '1';
        wait for 10ns;
    end process;
    
    process
    begin
        keys_i <= X"1000";
        wait for 20 ns;
        keys_i <= X"0000";
        wait for 20 ns;
        keys_i <= X"F002";
        wait;
    end process;

end Behavioral;
