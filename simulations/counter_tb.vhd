library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

library WORK;
use WORK.common.ALL;

entity counter_tb is
end counter_tb;

architecture Behavioral of counter_tb is
    
    signal clk: std_logic := '0';
    signal enable: std_logic := '0';
    signal reset: std_logic := '0';
    signal zero: std_logic := '0';
    signal data_out: unsigned(3 downto 0);
    signal data_in: unsigned(3 downto 0);
    signal load: std_logic;
    constant CLK_PERIOD: time := 10 ns;
begin
    counter_16bit: entity WORK.generic_counter 
                          generic map (WIDTH => 4)
                          port map (
                            clk => clk,
                            enable => enable,
                            reset => reset,
                            load => load,
                            data_in => data_in,
                            zero => zero,
                            data_out => data_out
                          );

    gen_clk: process
    begin
        clk <= '0';
        wait for CLK_PERIOD / 2;
        clk <= '1';
        wait for CLK_PERIOD / 2;
    end process;

    simulate: process
    variable count: unsigned(3 downto 0) := X"0";
    variable errors: integer := 0;
    begin
        data_in <= x"F";
        load <= '1';
        wait for CLK_PERIOD;
        load <= '0';        
        enable <= '0';
        wait for CLK_PERIOD;
        errors := 0;
        enable <= '1';
        report "Mod descrescator:";
        count := X"0";
        for i in 15 to 0 loop
            if (count /= data_out) then
                report "Valoare asteptata: " & integer'image(to_integer(count)) & 
                " Valoare obtinuta: " & integer'image(to_integer(data_out))
                severity ERROR;
                errors := errors + 1;
            end if;
            count:= count - 1;
            wait for CLK_PERIOD;
        end loop;
        
       
        
        if (errors = 0) then
            report "Simularea a avut succes";
        else
            report integer'image(errors) & " errrors";
        end if;
    
        wait;
    end process;
end Behavioral;
