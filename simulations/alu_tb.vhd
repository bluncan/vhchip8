library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

library WORK;
use WORK.common.ALL;

entity alu_tb is
end entity alu_tb;

architecture rtl of alu_tb is
    signal data_a: unsigned(7 downto 0);
    signal data_b: unsigned(7 downto 0);
    signal alu_op: ALUOp;
    signal data_out: unsigned(7 downto 0);
    signal zero: std_logic;
    signal carry_borrow: std_logic;

    constant WAITING_PERIOD : time := 10 ns;
begin
    alu: entity WORK.alu port map (
            data_a => data_a,
            data_b => data_b,
            alu_op => alu_op,
            data_out => data_out,
            zero => zero,
            carry => carry_borrow
        );

    test:process
    variable errors: integer := 0;
    variable expected: unsigned(7 downto 0);
    begin
        data_a <= X"04";
        data_b <= X"01";

        alu_op <= PASS_A;
        wait for WAITING_PERIOD;
        expected := data_a;
        if (data_out /= expected) then
            errors := errors + 1;
            report "PASS_A failed" 
                severity ERROR;
        end if;

        alu_op <= PASS_B;
        wait for WAITING_PERIOD;
        expected := data_b;
        if (data_out /= expected) then
            errors := errors + 1;
            report "PASS_B failed" 
                severity ERROR;
        end if;

        alu_op <= ADD;
        wait for WAITING_PERIOD;
        expected := data_a + data_b;
        if (data_out /= expected or carry_borrow /= '0') then
            errors := errors + 1;
            report "ADD failed" 
                severity ERROR;
        end if;
        
        -- test carry flag
        data_a <= x"ff";
        data_b <= x"04";
        wait for WAITING_PERIOD;
        expected := data_a + data_b;
        if (data_out /= expected or carry_borrow /= '1') then
            errors := errors + 1;
            report "ADD overflow failed" & " expected: " & integer'image(to_integer(expected)) & " got: " & integer'image(to_integer(data_out))
                severity ERROR;
        end if;

        alu_op <= SUB;
        wait for WAITING_PERIOD;
        expected := data_a - data_b;
        if (data_out /= expected or carry_borrow /= '1') then
            errors := errors + 1;
            report "SUB failed" 
                severity ERROR;
        end if;

        -- test borrow flag
        data_a <= x"00";
        data_b <= x"04";
        wait for WAITING_PERIOD;
        expected := data_a - data_b;
        if (data_out /= expected or carry_borrow /= '0') then
            errors := errors + 1;
            report "SUB underflow failed" & " expected: " & integer'image(to_integer(expected)) & " got: " & integer'image(to_integer(data_out))
                severity ERROR;
        end if;
        
        data_a <= X"04";
        data_b <= X"01";
        alu_op <= LEFT_SHIFT_ONE;
        wait for WAITING_PERIOD;
        expected := data_a(6 downto 0) & '0';
        if (data_out /= expected) then
            errors := errors + 1;
            report "LEFT_SHIFT failed" & " expected: " & integer'image(to_integer(expected)) & " got: " & integer'image(to_integer(data_out))
                severity ERROR;
        end if;

        alu_op <= RIGHT_SHIFT_ONE;
        wait for WAITING_PERIOD;
        expected := '0' & data_a(7 downto 1);
        if (data_out /= expected) then
            errors := errors + 1;
            report "RIGHT_SHIFT failed" 
                severity ERROR;
        end if;

        alu_op <= BITWISE_AND;
        wait for WAITING_PERIOD;
        expected := data_a and data_b;
        if (data_out /= expected) then
            errors := errors + 1;
            report "AND failed" 
                severity ERROR;
        end if;

        alu_op <= BITWISE_OR;
        wait for WAITING_PERIOD;
        expected := data_a or data_b;
        if (data_out /= expected) then
            errors := errors + 1;
            report "OR failed" 
                severity ERROR;
        end if;

        alu_op <= BITWISE_XOR;
        wait for WAITING_PERIOD;
        expected := data_a xor data_b;
        if (data_out /= expected) then
            errors := errors + 1;
            report "XOR failed" 
                severity ERROR;
        end if;

        -- test the zero flag
        data_a <= X"00";
        alu_op <= PASS_A;
        wait for WAITING_PERIOD;
        if (zero /= '1') then
            errors := errors + 1;
            report "Zero flag failed" 
                severity ERROR;
        end if;
        
        if (errors = 0) then
            report "Simulation succeeded" 
                severity NOTE;
        else
            report "Simulation failed with " & integer'image(errors) & " errors"
                severity ERROR;
        end if;
        
        wait;
    end process test;
end architecture;