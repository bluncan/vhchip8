library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity rng_tb is
end rng_tb;

architecture Behavioral of rng_tb is
    signal clk: std_logic;
    signal reset: std_logic;
    signal data_out: unsigned(7 downto 0);
    
    constant CLK_PERIOD: time := 20 ns;
begin
    rng_c: entity WORK.rng port map (
            clk => clk,
            reset => reset,
            enable => '1',
            data_out => data_out
           );
           
           
    test: process
        begin
            reset <= '1';
            wait for CLK_PERIOD;
            reset <= '0';
            wait;
        end process;
        
    gen_clk: process
        begin
            clk <= '0';
            wait for CLK_PERIOD / 2;
            clk <= '1';
            wait for CLK_PERIOD / 2;
        end process;
end Behavioral;
